const express = require('express')
const app = express()
const indexRoute = require('./routes/index')


app.use(express.static('public'))
app.set('view engine','ejs')


app.use('/',indexRoute)

const PORT = process.env.PORT || 8000

app.listen(PORT, () =>{
    console.log('server is running in port 8000')
})