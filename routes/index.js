const express = require('express')
const app = express()

app.get('/', (req,res) =>{
    res.render('index')
})
app.get('/users', (req,res) =>{
    res.render('users')
})
app.get('/save', (req,res) =>{
    res.render('save')
})
app.get('/pp', (req,res) =>{
    res.render('pp')
})
app.get('/createblog', (req,res) =>{
    res.render('createblog')
})
app.get('/feedback', (req,res) =>{
    res.render('feedback')
})
module.exports = app